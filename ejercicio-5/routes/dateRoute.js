const { Router } = require('express');
const {request ,  response } = 'express';

const router = Router();

const meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto', 'Septiembre','Octubre', 'Nomviembre','Diciembre'];
const  dias = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
const numero = (e) => {
    return e < 10 ? '0'+e : e;
}
  
router.get('/', (req, res = response) => {
    const date = new Date();
    res.json({
       
        day    : `${dias[date.getDay()]}`,
        date_1 : `${date.getDate()} de ${meses[date.getMonth()]}`,
        year   : date.getFullYear(),
        date_2 : `${numero(date.getDate())}/${numero(date.getMonth()+1)}/${date.getFullYear()}`,
        hour   : `${numero(date.getHours())}:${numero(date.getMinutes())}:${numero(date.getSeconds())}`,
        prefix : `${date.getHours() > 11? 'PM': 'AM'}`
        
    })
});
//date.prefix
module.exports = router;