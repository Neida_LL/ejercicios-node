const { Router } = require('express');
const { pokemonGet, covidGet,typicodeGet } = require('../controllers/consumirController');

const router = Router();

router.get('/pokemon', pokemonGet);

router.get('/covid', covidGet);

router.get('/typicode', typicodeGet);


module.exports = router;