const { default: axios } = require('axios');
const { guardarDB} = require('../helpers/guardarDB');

const {request ,  response } = 'express';

const urlP ='https://pokeapi.co/api/v2/type/3';
const urlC ='https://api.covidtracking.com/v1/us/daily.json';
const urlT ='https://jsonplaceholder.typicode.com/users';




const pokemonGet = async (req, res = response) => {
  try {
    const jsonResponse =await axios.get(urlP);
    const response = jsonResponse.data;
    Object.keys(response).forEach(key =>{
      console.log(key);
    })
    guardarDB(response,1);
    res.status(200).json({
      response
    });
   } catch (error) {
     console.log(error);
     res.status.json({
       error :'hubo un error al consumir el servicio'
     })
   } 
};

const covidGet = async (req, res = response) => {
  try {
    const jsonResponse =await axios.get(urlC);
    const response = jsonResponse.data;
    guardarDB(response,2);
    res.status(200).json({
      response
    });
   } catch (error) {
     console.log(error);
     res.status.json({
       error :'hubo un error al consumir el servicio'
     })
   }
} ;

const typicodeGet = async (req, res = response) => {
  try {
    const jsonResponse =await axios.get(urlT);
    const response = jsonResponse.data;
    guardarDB(response,3);
    res.status(200).json({
      response
    });
   } catch (error) {
     console.log(error);
     res.status.json({
       error :'hubo un error al consumir el servicio'
     })
   }
}; 



module.exports = {
  pokemonGet,
  covidGet,
  typicodeGet
}