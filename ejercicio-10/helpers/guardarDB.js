const fs =require('fs');

const archivoP ='./resultado-endpoints/dataPokemon.txt';
const archivoC ='./resultado-endpoints/dataCovid.txt';
const archivoT ='./resultado-endpoints/dataTypicode.txt';

const guardarDB = (data ,nro) => {
  // console.log(data);

  switch (nro) {
    case 1:
      fs.writeFileSync(archivoP ,JSON.stringify(data));
    break;
    case 2:
      fs.writeFileSync(archivoC ,JSON.stringify(data));
    break;
    default:
      fs.writeFileSync(archivoT ,JSON.stringify(data));
      break;
  }
}

module.exports ={
  guardarDB,
}