const { Router } = require('express');
const { productosGet, productoPut, productoPost, productoDelete, productoGetByNroSDate, productoGetRealcion } = require('../controllers/productoControllers');

const router = Router();

router.get('/', productosGet);
router.get('/:parametro', productoGetByNroSDate);
router.get('/:id/persona', productoGetRealcion);

router.put('/:id', productoPut);

router.post('/', productoPost);

router.delete('/:id', productoDelete);

module.exports = router;