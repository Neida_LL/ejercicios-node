const { guardarDBProducto, getDBProducto , getDBPersona} = require('../helpers/guardarDB');
const Producto = require("../models/producto");
const Productos = require("../models/productos");
const Personas = require("../models/personas");

const {request ,  response } = 'express';

const productos = new Productos();
const personas = new Personas();

const productosGet = (req, res = response) => {
  const productosDB =  getDBProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
  res.json({
    productosDB
  });
  // let array =   productos._listado;
};

const productoGetByNroSDate = (req, res = response) => {
  const productosDB =  getDBProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
  const {parametro} = req.params;
  let product;
  if (parametro) {
    product = productos.searchByParams(parametro);
    res.json({
      product
    });
  } 
 // let array =   productos._listado;
};



const productoGetRealcion = (req, res = response) => {
  const productosDB =  getDBProducto();
  const personasDB =  getDBPersona();
  let product;
  let persona;

  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  const {id} = req.params;
  
  if (id) {
    product = productos.getProducto(id);
    console.log(product);
    if (product) {
      persona = personas.getPersona(product.Id_persona);
      if (persona) {
        res.json({
          product,
          persona
        });    
      }else{
        res.json({
          msg:'No existe la persona relacionada a este preoducto',
        });    
      }
    }else{
      res.json({
        msg:`No existe un producto con el id "${id}"`
      });
    }
  }
}




const productoPost = (req, res = response) => {
 const { Nombre , Categoria, Nro_serie, Fecha_caducidad,Id_persona } = req.body;
  const producto = new Producto(Nombre , Categoria, Nro_serie, Fecha_caducidad,Id_persona);
  let productosDB =  getDBProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
  productos.crearProducto(producto);
  
   console.log(productos._listado);
  guardarDBProducto(productos.listadoArr);
  productosDB =  getDBProducto();
  res.json({
    msg: 'post API - Controlador',
    productosDB
  });
};

const productoPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    productos.borrarProducto(id);
    const { Nombre, Categoria, Nro_serie ,Fecha_caducidad,Id_persona} = req.body;
    const producto = new Producto(Nombre, Categoria, Nro_serie ,Fecha_caducidad,Id_persona);
    producto.setID(id);
    productos.crearProducto(producto);
    guardarDBProducto(productos.listadoArr);
  }
  res.json({
    msg: 'put API - Controlador'
  });
};

//Crear y mandar informacion  con POST:


const productoDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    productos.borrarProducto(id);
    guardarDBProducto(productos.listadoArr);
  }
  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  productosGet,
  productoGetByNroSDate,
  productoGetRealcion,
  productoPut,
  productoPost,
  productoDelete
}