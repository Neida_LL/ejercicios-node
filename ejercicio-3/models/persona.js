const { v4: uuidv4 } = require('uuid');

class Persona {
  
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;
  id_Producto ='';

  constructor(nombres, apellidos, ci,idP) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    this.id_Producto =idP;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Persona;