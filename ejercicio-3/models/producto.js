/*bussines intelligence :
Reporte,
analisis de datos,
*/ 
const { v4: uuidv4 } = require('uuid');

class Producto {
  
  Id = '';
  Nombre = '';
  Categoria = '';
  Nro_serie = 0;
  Fecha_caducidad='';
  Id_persona='';

  constructor(nombre, categoria, nroSerie,fechaCaducidad,idPersona) {
    this.Id = uuidv4();
    this.Nombre = nombre;
    this.Categoria = categoria;
    this.Nro_serie = nroSerie;
    this.Fecha_caducidad =fechaCaducidad;
    this.Id_persona =idPersona;
  }

  setID(idx){
    this.Id =idx;
  }
}

module.exports = Producto;