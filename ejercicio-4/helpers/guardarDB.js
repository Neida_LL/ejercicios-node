const fs =require('fs');

const archivo ='./db/dataExamen.json';
const archivoEst ='./db/dataProducto.json';
const archivoDoc ='./db/dataProducto.json';

const guardarDBExamen = (data) => {
  // console.log(data);
  fs.writeFileSync(archivo ,JSON.stringify(data));
}

const getDBExamen = () =>{
  if (!fs.existsSync(archivo)) {
    return null;
  }
  //leer el archivo :
  fs
  const info = fs.readFileSync(archivo, {encoding: 'utf-8'});
  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}

//------------------Estudiante----------------------------

const guardarDBEstudiante = (data) => {
  // console.log(data);
  fs.writeFileSync(archivoEst ,JSON.stringify(data));
}

const getDBEstudiante = () =>{
  if (!fs.existsSync(archivoEst)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivoEst, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}


//------------------Docente-----------------------
const guardarDBDocente= (data) => {
  // console.log(data);
  fs.writeFileSync(archivoDoc ,JSON.stringify(data));
}

const getDBDocente= () =>{
  if (!fs.existsSync(archivoDoc)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivoDoc, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}



module.exports ={
  guardarDBExamen,
  getDBExamen,
  guardarDBEstudiante,
  getDBEstudiante,
  getDBDocente,
  guardarDBDocente
}