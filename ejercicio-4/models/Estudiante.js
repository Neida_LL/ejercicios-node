const { v4: uuidv4 } = require('uuid');

class Estudiante {
  
  id        = '';
  nombres   = '';
  apellidos = '';
  ci        = 0 ;
  carrera   = '';
  Id_examen = '';

  constructor(nombres, apellidos, ci,carrera,id_exam) {
    this.id        = uuidv4();
    this.nombres   = nombres;
    this.apellidos = apellidos;
    this.ci        = ci;
    this.carrera   = carrera;
    this.Id_examen = id_exam;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Estudiante;