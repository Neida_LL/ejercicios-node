const { v4: uuidv4 } = require('uuid');

class Docente {
  
  id         = '';
  nombres    = '';
  apellidos  = '';
  asignatura = '';
  Id_examen  = '';

  constructor(nombres, apellidos, asignatura,idP) {
    this.id         = uuidv4();
    this.nombres    = nombres;
    this.apellidos  = apellidos;
    this.asignatura = asignatura;
    this.Id_examen  = idP;
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Docente;