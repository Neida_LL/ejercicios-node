
class Estudiantes {

  constructor() {
    this._listado = [];
  }

  
  getEstudiante(id){
    return this._listado[id];
  }


  get listadoArr(){
    const listado  =[];
    Object.keys(this._listado).forEach(key =>{
      const estudiante = this._listado[key];
      listado.push(estudiante);
    })
    console.log(listado);
    return listado;
  }
 
  crearEstudiante(estudiante = {}) {
    this._listado[estudiante.id] = estudiante;
  }

  cargarEstudiantesEnArray(estudiantes=[]){
    estudiantes.forEach(estudiante =>{
    this._listado[estudiante.id]=estudiante;
    })
  }

  borrarEstudiante(id ){
    delete this._listado[id];
  }

  searchByNames(word){
    let estudiante;
    estudiante = this.listadoArr.find(e => e.nombres == word);
    if (estudiante) {
      return estudiante
    }else{
      estudiante = this.listadoArr.find(e =>e.apellidos ==word);
      if (estudiante) {
        return estudiante;
      }else{
        return  `El apellido o nombre "${word}" no existe`;
      }
    }

  }

}

module.exports = Estudiantes;