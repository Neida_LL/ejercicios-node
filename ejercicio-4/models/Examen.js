const { v4: uuidv4 } = require('uuid');

class Examen{

  id            = '';
  Nombre        = '';
  Fecha         = '';
  Nota          = 0; 
  Id_docente    = '';
  Id_Estudiante = '';
  
  constructor(name,fecha,nota,id_doc,id_est){
    this.id            = uuidv4();
    this.Nombre        = name;
    this.Fecha         = fecha;
    this.Nota          = nota;
    this.Id_docente    = id_doc;
    this.Id_Estudiante = id_est
  }

}

module.exports = Examen;
