const { guardarDBDocente, getDBDocente } = require('../helpers/guardarDB');
const {request ,  response } = 'express';

const Docente = require("../models/docente");
const Docentes = require("../models/docentes");


const docentes = new Docentes();

const docentesGet = (req, res = response) => {
  const docentesDB =  getDBDocente();
  if (docentesDB) {
    docentes.cargardocentesEnArray(docentesDB);
  }
  res.json({
    docentesDB
  });
};


const docentePost = (req, res = response) => {
 const { nombres, apellidos, asignatura, Id_examen } = req.body;
  const docente = new Docente( nombres, apellidos, asignatura, Id_examen);
  let docentesDB =  getDBDocente();
  if (docentesDB) {
    docentes.cargarDocentesEnArray(docentesDB);
  }
  docentes.crearDocente(docente);
  guardarDBDocente(docentes.listadoArr);
  docentesDB =  getDBDocente();
  res.json({
    docentesDB
  });
};

const docentePut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    docentes.borrarDocente(id);
    const { nombres, apellidos, asignatura, Id_examen} = req.body;
    const docente = new Docente(nombres, apellidos, asignatura, Id_examen);
    docente.setID(id);
    docentes.crearDocente(docente);
    guardarDBDocente(docentes.listadoArr);
  }
  res.json({
    msg: 'Actualizado correctamente'
  });
};

const docenteDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    docentes.borrarDocente(id);
    guardarDBDocente(docentes.listadoArr);
  }
  res.json({
    msg: 'Eliminado con exito'
  });
};

module.exports = {
  docentesGet,
  docentePut,
  docentePost,
  docenteDelete
}