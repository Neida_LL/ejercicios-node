const { guardarDBExamen, getDBExamen } = require('../helpers/guardarDB');
const Examen = require("../models/Examen");
const Examenes = require("../models/examenes");

const {request ,  response } = 'express';

const examenes = new Examenes();

const examenesGet = (req, res = response) => {
  const examenesDB =  getDBExamen();
  if (examenesDB) {
    examenes.cargarExamenesEnArray(examenesDB);
  }
  res.json({
    examenesDB
  });
};


const examenPost = (req, res = response) => {
 const { Nombre , Fecha, Nota, Id_docente,Id_Estudiante } = req.body;
  const examen = new Examen( Nombre , Fecha, Nota, Id_docente,Id_Estudiante);
  let examenesDB =  getDBExamen();
  if (examenesDB) {
    examenes.cargarExamenesEnArray(examenesDB);
  }
  examenes.crearExamen(examen);
  
   console.log(examenes._listado);
  guardarDBExamen(examenes.listadoArr);
  examenesDB =  getDBExamen();
  res.json({
    examenesDB
  });
};

const examenPut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    examenes.borrarExamen(id);
    const {  Nombre , Fecha, Nota, Id_docente,Id_Estudiante} = req.body;
    const examen = new Examen( Nombre , Fecha, Nota, Id_docente,Id_Estudiante);
    examen.setID(id);
    examenes.crearExamen(examen);
    guardarDBExamen(examenes.listadoArr);
  }
  res.json({
    msg: 'Actualizado correctamente'
  });
};


const examenDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    examenes.borrarExamen(id);
    guardarDBExamen(examenes.listadoArr);
  }
  res.json({
    msg: 'Eliminado correctamente'
  });
};

module.exports = {
  examenesGet,
  examenPut,
  examenPost,
  examenDelete
}