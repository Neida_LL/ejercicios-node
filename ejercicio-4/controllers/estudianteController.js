const { guardarDBEstudiante, getDBEstudiante } = require('../helpers/guardarDB');
const Estudiante = require("../models/estudiante");
const Estudiantes = require("../models/estudiantes");

const {request ,  response } = 'express';

const estudiantes = new Estudiantes();

const estudiantesGet = (req, res = response) => {
  const estudiantesDB =  getDBEstudiante();
  if (estudiantesDB) {
    estudiantes.cargarEstudiantesEnArray(estudiantesDB);
  }
  res.json({
    estudiantesDB
  });
};


const estudiantePost = (req, res = response) => {
 //Actualizando lista de Personas
  let estudiantesDB =  getDBEstudiante();
  if (estudiantesDB) {
    estudiantes.cargarEstudiantesEnArray(estudiantesDB);
  }
  const {nombres, apellidos, ci,carrera,Id_examen} = req.body;
  const estudiante = new Estudiante(nombres, apellidos, ci,carrera,Id_examen);
  estudiantes.crearEstudiante(estudiante);
  // console.log(listado);
  guardarDBEstudiante(estudiantes.listadoArr);
  estudiantesDB =  getDBEstudiante();
  res.json({
    estudiantesDB
  });
};

const estudiantePut = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    estudiantes.borrarEstudiante(id);
    const { nombres, apellidos, ci,carrera,Id_examen} = req.body;
    const estudiante = new Estudiante(nombres, apellidos, ci,carrera,Id_examen);
    estudiante.setID(id);
    estudiantes.crearEstudiante(estudiante);
    guardarDBEstudiante(estudiantes.listadoArr);
  }
  res.json({
    msg: 'Se actualizo exitosamente'
  });
};


const estudianteDelete = (req, res = response) => {
  const {id} = req.params;
  if (id) {
    estudiantes.borrarEstudiante(id);
    guardarDBEstudiante(estudiantes.listadoArr);
  }
  res.json({
    msg: 'Se elimino correctamente'
  });
};

module.exports = {
  estudiantesGet,
  estudiantePut,
  estudiantePost,
  estudianteDelete
}