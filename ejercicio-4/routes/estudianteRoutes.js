const { Router } = require('express');
const { estudiantesGet, estudiantePut, estudiantePost, estudianteDelete} = require('../controllers/estudianteController');

const router = Router();

router.get('/', estudiantesGet);

router.put('/:id', estudiantePut);

router.post('/', estudiantePost);

router.delete('/:id', estudianteDelete);

module.exports = router;