const { Router } = require('express');
const { personasGet, personaPut, personaPost, personaDelete, personaGetByCI, personasGetByLastNameAndGener } = require('../controllers/personaController');

const router = Router();

router.get('/', personasGet);
router.get('/:CI', personaGetByCI);
router.get('/:x/:sexo', personasGetByLastNameAndGener);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);

module.exports = router;