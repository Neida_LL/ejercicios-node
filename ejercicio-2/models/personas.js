const Persona = require("./persona");

class Personas {

  constructor() {
    this._listado = [];
  }

  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const persona = this._listado[key];
      //console.log(persona);
      listado.push(persona);
    })
 
    return listado;
  }
 
  crearPersona(persona = {}) {
    // this._listado.push(persona);
    /// return this._listado;
    this._listado[persona.id] = persona;
  }

  cargarPersonasEnArray(personas=[]){
   //console.log(personas);
   personas.forEach(persona =>{
     this._listado[persona.id]=persona;
   })

   //console.log(this._listado);
  }

  borrarPersona(id ){
    console.log(this._listado);
    delete this._listado[id];
    console.log(this._listado);

  }

  buscarPersona(nombres, apellidos){
    let persona = [];
     persona = this.listadoArr.filter(element => element.nombres == nombres && element.apellidos == apellidos);
    return persona;
  }
 
  obtenerFiltro(x, sexo){
    let personas = [];
    this.listadoArr.forEach(e => {
      console.log(e.apellidos);
      if (e.apellidos.charAt(0).toUpperCase() == x.toUpperCase() && e.sexo.toUpperCase() == sexo.toUpperCase()) {
        console.log(e.apellidos);
        personas.push(e);
      }
    });
    console.log(personas);
    return personas;
  }

  buscarCIPerson(atributo){
    let persona =[];
    
    if (atributo.toUpperCase() =="F"|| atributo.toUpperCase() =="M" ) {
      this.listadoArr.forEach(e => {
        if (e.sexo == atributo.toUpperCase()) {
          persona.push(e);
        }
      });
    } else {
      // console.log();
      this.listadoArr.forEach(e => {
        if (e.ci == atributo) {
          persona.push(e);
        }
      });
    }
    return persona;
  }
}

module.exports = Personas;