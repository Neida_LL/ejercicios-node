const { guardarDB, getDB } = require('../helpers/guardarDB');
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const {request ,  response } = 'express';

const personas = new Personas();

const personasGet = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
   const {nombres, apellidos} = req.query
   console.log(nombres,apellidos);
   let persona ;
   if (nombres && apellidos) {
      persona = personas.buscarPersona(nombres,apellidos);
      res.json({
        persona
      });
   }else{
      res.json({
        personasDB
      });
   }
  // let array =   personas._listado;
};

const personaGetByCI = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  const {CI} =req.params
  let persona;
  if (CI) {
    persona = personas.buscarCIPerson(CI);
      res.json({
        persona
      });
    
  }else{
    res.json({
      personasDB
    });
  }
} ;


const personasGetByLastNameAndGener = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  const  {x , sexo} = req.params;
  let lista;
   if (x && sexo) {
     console.log(x , sexo);
       lista = personas.obtenerFiltro(x, sexo); 
       if (lista !==[]) {
         res.json({
           lista
         });
       }else{
         res.json({
           msg: 'No existe persona con esas caracteristicas',
         });
       }
   }
}; 

const personaPost = (req, res = response) => {
 const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  personas.crearPersona(persona);
  guardarDB(personas.listadoArr);
  personasDB =  getDB();
  res.json({
    personasDB
  });
};

const personaPut = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }
  res.json({
    msg: 'Actualizado correctamente'
  });
};


const personaDelete = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
  const {id} = req.params;
  if (id) {
    personas.borrarPersona(id);
    guardarDB(personas.listadoArr);
  }

  res.json({
    msg: 'Eliminado con exito'
  });
};

module.exports = {
  personasGet,
  personaGetByCI,
  personasGetByLastNameAndGener,
  personaPut,
  personaPost,
  personaDelete
}